<?php

namespace Drupal\commerce_eurobank_redirect\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "eurobank_redirect",
 *   label = "Eurobank Payment Redirect",
 *   display_label = "Eurobank Payment Redirect",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_eurobank_redirect\PluginForm\OffsiteRedirect\EurobankPaymentRedirectForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class EurobankPaymentRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'version' => '2',
      'mid' => '0000000000',
      'currency' => 'EUR',
      'confirmUrl' => 'https://example.com/commerce_eurobank_redirect/callback?action=success',
      'cancelUrl' => 'https://example.com/commerce_eurobank_redirect/callback?action=failure',
      'shared_secret' => 'SECRET',
      'postUrl' => 'https://euro.test.modirum.com/vpos/shophandlermpi',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['version'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Specification version'),
      '#description' => $this->t('Value 2'),
      '#default_value' => $this->configuration['version'],
    ];

    $form['mid'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Merchant id supplied (integer number) will be supplied to merchant, max length 30'),
      '#default_value' => $this->configuration['mid'],
    ];

    $form['currency'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Currency'),
      '#description' => $this->t('Order amount currency (string 3 ISO ISO 4217 alphabetic code (EUR, USD))'),
      '#default_value' => $this->configuration['currency'],
    ];

    $form['confirmUrl'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Confirm url'),
      '#description' => $this->t('Confirmation url where to send payment confirmation in case payment was successful (string..128)'),
      '#default_value' => $this->configuration['confirmUrl'],
    ];

    $form['cancelUrl'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Cancel url'),
      '#description' => $this->t('Cancel url where to send payment feedback in case payment has failed or was canceled by user (string..128)'),
      '#default_value' => $this->configuration['cancelUrl'],
    ];

    $form['shared_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Shared Secret'),
      '#description' => $this->t('Shared Secret'),
      '#default_value' => $this->configuration['shared_secret'],
    ];

    $form['postUrl'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Post url'),
      '#description' => $this->t('Post url'),
      '#default_value' => $this->configuration['postUrl'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['version'] = $values['version'];
      $this->configuration['mid'] = $values['mid'];
      $this->configuration['currency'] = $values['currency'];
      $this->configuration['confirmUrl'] = $values['confirmUrl'];
      $this->configuration['cancelUrl'] = $values['cancelUrl'];
      $this->configuration['shared_secret'] = $values['shared_secret'];
      $this->configuration['postUrl'] = $values['postUrl'];
    }
  }

}
