<?php

namespace Drupal\commerce_eurobank_redirect\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides the redirect form.
 */
class EurobankPaymentRedirectForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a EurobankPaymentRedirectForm object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get('commerce_eurobank_redirect');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $payment_gateway_plugin->getConfiguration();
    $order = $payment->getOrder();
    // Format amount.
    $amount = sprintf('%0.2f', $order->getTotalPrice()->getNumber());

    // Save response data for validating response later.
    $order->setData("EurobankGatewayData", [
      "shared_secret" => $config["shared_secret"],
      "payment_gateway" => $payment->getPaymentGateway()->id(),
    ]);
    $order->save();

    // Calculate digest.
    $digest_data = "";
    $digest_data_array = [];

    $digest_data_array[1] = $config['version'];
    $digest_data_array[2] = $config['mid'];
    $digest_data_array[3] = "";
    $digest_data_array[4] = "";
    $digest_data_array[5] = $order->id();
    $digest_data_array[6] = "";
    $digest_data_array[7] = $amount;
    $digest_data_array[8] = $config['currency'];
    $digest_data_array[9] = "";
    $digest_data_array[10] = "";
    $digest_data_array[11] = "GR";
    $digest_data_array[12] = $order->getBillingProfile()->get('address')->locality;
    $digest_data_array[13] = $order->getBillingProfile()->get('address')->postal_code;
    $digest_data_array[14] = $order->getBillingProfile()->get('address')->locality;
    $digest_data_array[15] = $order->getBillingProfile()->get('address')->address_line1;
    $digest_data_array[16] = "";
    $digest_data_array[17] = "";
    $digest_data_array[18] = "";
    $digest_data_array[19] = "";
    $digest_data_array[20] = "";
    $digest_data_array[21] = "";
    $digest_data_array[22] = "";
    $digest_data_array[23] = "";
    $digest_data_array[24] = "";
    $digest_data_array[25] = "";
    $digest_data_array[26] = "";
    $digest_data_array[27] = "";
    $digest_data_array[28] = "";
    $digest_data_array[29] = "";
    $digest_data_array[30] = "";
    $digest_data_array[31] = "";
    $digest_data_array[32] = "";
    $digest_data_array[33] = "";
    $digest_data_array[34] = $config['confirmUrl'];
    $digest_data_array[35] = $config['cancelUrl'];
    $digest_data_array[36] = "";
    $digest_data_array[37] = "";
    $digest_data_array[38] = "";
    $digest_data_array[39] = "";
    $digest_data_array[40] = "";
    $digest_data_array[41] = "";
    $digest_data_array[42] = "";
    $digest_data_array[43] = "";
    $digest_data_array[44] = "";
    $digest_data_array[45] = $config['shared_secret'];

    $digest_data = implode("", $digest_data_array);
    $digest = base64_encode(hash("sha256", $digest_data, TRUE));

    // Prepare redirect form.
    $data = [
      'version' => $digest_data_array[1],
      'mid' => $digest_data_array[2],
      'lang' => $digest_data_array[3],
      'deviceCategory' => $digest_data_array[4],
      'orderid' => $digest_data_array[5],
      'orderDesc' => $digest_data_array[6],
      'orderAmount' => $digest_data_array[7],
      'currency' => $digest_data_array[8],
      'payerEmail' => $digest_data_array[9],
      'payerPhone' => $digest_data_array[10],
      'billCountry' => $digest_data_array[11],
      'billState' => $digest_data_array[12],
      'billZip' => $digest_data_array[13],
      'billCity' => $digest_data_array[14],
      'billAddress' => $digest_data_array[15],
      'weight' => $digest_data_array[16],
      'dimensions' => $digest_data_array[17],
      'shipCountry' => $digest_data_array[18],
      'shipState' => $digest_data_array[19],
      'shipZip' => $digest_data_array[20],
      'shipCity' => $digest_data_array[21],
      'shipAddress' => $digest_data_array[22],
      'addFraudScore' => $digest_data_array[23],
      'maxPayRetries' => $digest_data_array[24],
      'reject3dsU' => $digest_data_array[25],
      'payMethod' => $digest_data_array[26],
      'trType' => $digest_data_array[27],
      'extInstallmentoffset' => $digest_data_array[28],
      'extInstallmentperiod' => $digest_data_array[29],
      'extRecurringfrequency' => $digest_data_array[30],
      'extRecurringenddate' => $digest_data_array[31],
      'blockScore' => $digest_data_array[32],
      'cssUrl' => $digest_data_array[33],
      'confirmUrl' => $digest_data_array[34],
      'cancelUrl' => $digest_data_array[35],
      'var1' => $digest_data_array[36],
      'var2' => $digest_data_array[37],
      'var3' => $digest_data_array[38],
      'var4' => $digest_data_array[39],
      'var5' => $digest_data_array[40],
      'var6' => $digest_data_array[41],
      'var7' => $digest_data_array[42],
      'var8' => $digest_data_array[43],
      'var9' => $digest_data_array[44],
      'digest' => $digest,
    ];

    $form = $this->buildRedirectForm($form, $form_state, $config['postUrl'], $data, 'post');
    return $form;
  }

}
