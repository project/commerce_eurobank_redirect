<?php

namespace Drupal\commerce_eurobank_redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Endpoints for the routes defined.
 */
class CallbackController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a CallbackController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, TimeInterface $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('commerce_eurobank_redirect');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * Callback action.
   *
   * Listen for callbacks from QuickPay and creates any payment specified.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function callback(Request $request) {
    $message = $this->processCallback($request);
    return [
      '#type' => 'markup',
      '#markup' => $message,
    ];
  }

  /**
   * Process the callback from eurobank.
   */
  public function processCallback(Request $request) {
    $resultCode = $request->get('status');
    if ($resultCode !== "CAPTURED" && $resultCode !== "AUTHORIZED") {
      $this->logResponse($request, "failure");
      return $this->getDisplayMessage($request);
    }

    $digest = $request->get('digest');
    $order_id = $request->get('orderid');
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);

    $hash = $this->calculateHash($request, $order_id);
    if ($hash !== $digest) {
      $this->createPayment($order, $request, "Unvalidated");
      $this->logResponse($request, "failure");
      return $this->t('Sorry we were not able to validate your payment');
    }
    $this->createPayment($order, $request);
    $this->logResponse($request, "success");
    return $this->getDisplayMessage($request);
  }

  /**
   * Log response data to database log and order object.
   */
  public function logResponse($request, $status = "success") {
    $order_id = $request->get('orderid');
    $data = [
      "txId" => $request->get('txId'),
      "orderid" => $order_id,
      "status" => $request->get('status'),
    ];

    // Log for immediate action.
    if ($status === "success") {
      $this->logger->info("Payment " . $status . ": " . '<pre><code>' . print_r($data, TRUE) . '</code></pre>');
    }
    elseif ($status === "failure") {
      $this->logger->alert("Payment " . $status . ": " . '<pre><code>' . print_r($data, TRUE) . '</code></pre>');
    }

    // Log in the order data field for future reference.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    $eurobankGatewayData = $order->getData("EurobankGatewayData");
    $eurobankGatewayData = array_merge($eurobankGatewayData, $data);
    $order->setData("EurobankGatewayData", $eurobankGatewayData);
    $order->save();
  }

  /**
   * Create Payment.
   */
  public function createPayment(OrderInterface $order, Request $request, $state = "completed") {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $gateway_data = $order->getData("EurobankGatewayData");
    /** @var \Drupal\commerce_payment\Entity $payment */
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getBalance(),
      'payment_gateway' => $gateway_data["payment_gateway"],
      'order_id' => $order->id(),
      'remote_state' => $request->get('status'),
      'message' => $request->get('message'),
      'payMethod' => $request->get('payMethod'),
      'remote_id' => $request->get('txId'),
      'paymentRef' => $request->get('paymentRef'),
    ]);

    /*if ($state == "completed") {
    $payment->setAuthorizedTime(REQUEST_TIME);
     */
    $payment->setCompletedTime($this->time->getRequestTime());
    /* } */
    $payment->save();
    $this->messenger()->addMessage($this->t('Your payment was successful with order_id : @order_id', [
      '@order_id' => $order->id(),
    ]));
  }

  /**
   * Calculates hash key by concatenation of values, then uses sha256 algorithm.
   *
   * @return string
   *   The hash.
   */
  public function calculateHash($request, $order_id) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
    $gateway_data = $order->getData("EurobankGatewayData");
    $post_data_array = [];
    $post_data_array[0] = $request->get('version');
    $post_data_array[1] = $request->get('mid');
    $post_data_array[2] = $order_id;
    $post_data_array[3] = $request->get('status');
    $post_data_array[4] = $request->get('orderAmount');
    $post_data_array[5] = $request->get('currency');
    $post_data_array[6] = $request->get('paymentTotal');
    $post_data_array[7] = $request->get('message');
    $post_data_array[8] = $request->get('riskScore');
    $post_data_array[9] = $request->get('payMethod');
    $post_data_array[10] = $request->get('txId');
    $post_data_array[11] = $request->get('Sequence');
    $post_data_array[12] = $request->get('SeqTxId');
    $post_data_array[13] = $request->get('paymentRef');
    $post_data_array[14] = $gateway_data["shared_secret"];
    $post_data = implode("", $post_data_array);
    return base64_encode(hash("sha256", $post_data, TRUE));
  }

  /**
   * Get message.
   *
   * @return string
   *   The message.
   */
  public function getDisplayMessage($request) {
    $messages = [
      'AUTHORIZED,' => $this->t('Payment was successful (accept order)'),
      'CAPTURED' => $this->t('Payment was successful (accept order)'),
      'CANCELED' => $this->t('Payment failed, user canceled the process (deny order)'),
      'REFUSED' => $this->t('Payment failed, payment was denied for card or by bank (deny order)'),
      'ERROR' => $this->t('Non recoverable system or other error occurred during payment process (deny order)'),
    ];
    $message = $messages[$request->get('status')];
    return $message;
  }

}
