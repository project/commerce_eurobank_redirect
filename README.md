CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Testing connection
 * Local mock server
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

Provides payment gateway for Drupal 8 commerce. It is a redirection for Eurobank
of Greece.

By using redirection payments your clients get redirected to the bank and fill
in their credit card data there. No information is stored at the Drupal side.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_eurobank_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_eurobank_redirect

RECOMMENDED MODULES
-------------------

 * No extra modules are required.

INSTALLATION
------------

 * Install as usual, see https://www.drupal.org/docs/8/extending-drupal-8
   installing-contributed-modules-find-import-enable-configure-drupal-8 for
   further information.

CONFIGURATION
-------------

 * Go to `Commerce > configuration > payment > payment gateways` and add a new
   method. Choose Eurobank from the options provided and fill in the required
   fields which will be given with the bank aggreement.

TESTING CONNECTION
---------------

Bank should provide testing endpoints as soon as the bank contract aggreement is
signed. You will be asked to provide the following to the bank:

```
Web site URL: https://yourdomain.com
Referrer URL: https://yourdomain.com/commerce_eurobank_redirect/callback
Success URL: https://yourdomain.com/commerce_eurobank_redirect/callback?action=success
Failure URL: https://yourdomain.com/commerce_eurobank_redirect/callback?action=cancel
```


MAINTAINERS
-----------

Current maintainers:

 * Konstantinos Skarlatos (https://www.drupal.org/user/1869414/)
